from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from .forms import ProductForm
from .models import Product

# Create your views here.

def page1(request):
    return render(request, 'home/myblog.html')

def page2(request):
    return render(request, 'home/gallery.html')

def page3(request):
    return render(request, 'home/story1.html')

def list(request):
    listp = Product.objects.all()
    return render(request, 'home/product_list.html' , {'listp':listp})

def product_create_view(request):
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/ListProduct')
    return render(request, 'home/product_create.html', {'form':form})

def delete(request,id):
    item = Product.objects.get(id=id)
    if request.method == 'POST':
        item.delete()
        return redirect('/ListProduct')
    return render(request, 'home/product_delete.html', {'item':item})

def detail(request,id):
    prod = Product.objects.get(id=id)
    return render (request, 'home/product_detail.html',{'prod':prod})

def update(request, id):
    prod = Product.objects.get(id=id)
    form = ProductForm(instance=prod)
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=prod)
        if form.is_valid():
            form.save()
            return redirect('/ListProduct')

    return render(request, 'home/product_create.html', {'form':form})