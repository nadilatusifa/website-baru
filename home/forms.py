from django import forms

from .models import Product

class ProductForm(forms.ModelForm):
    namaMatkul = forms.CharField(label='Nama Matkul :')
    dosen      = forms.CharField(label='Dosen   :')
    jumlahSks  = forms.IntegerField(label='Jumlah SKS :')
    semester   = forms.CharField(label='Semester  :')
    ruangKelas = forms.CharField(label='Ruang Kelas :')
    
    class Meta:
        model = Product
        fields = '__all__'