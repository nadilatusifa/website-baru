from django.urls import path

from . import views
app_name = 'home'

urlpatterns = [
    path('', views.page1, name='home'),
    path('gallery', views.page2, name='gallery'),
    path('story1', views.page3, name='story1'),
    path('FormProduct',views.product_create_view, name= 'product_create'),
    path('ListProduct', views.list, name='product_list'),
    path('delete/<str:id>', views.delete, name="product_delete"),
    path('<int:id>',views.detail),
    path('update/<str:id>', views.update, name='update')
]