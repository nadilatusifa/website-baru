from django.db import models

# Create your models here.
class Product(models.Model):
    namaMatkul = models.CharField(max_length = 120)
    dosen      = models.CharField(max_length = 120)
    jumlahSks  = models.IntegerField()
    semester   = models.CharField(max_length = 120)
    ruangKelas = models.CharField(max_length = 120)

    def __str__(self):
        return self.namaMatkul