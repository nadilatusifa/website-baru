from django.urls import path

from . import views
app_name = 'acordion'

urlpatterns = [
    path("acordion", views.mini_profile_view, name="mini_profile")
]