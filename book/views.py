from django.shortcuts import render,get_object_or_404, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import urllib3
import requests
import json

# Create your views here.

@login_required(login_url="login:logging_in")
def index(request):
    response = {
        "namanya" : request.user.username
    }

    return render(request, 'book/index.html', response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)     