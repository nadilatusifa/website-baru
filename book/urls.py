from django.urls import path

from . import views
app_name = 'book'

urlpatterns = [
    path("book", views.index, name="book"),
    path("data/", views.fungsi_data)
]