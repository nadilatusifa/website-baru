
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm

# Create your views here.
def register(request):
    if request.user.is_authenticated:
        return redirect("book:book")
    context = {
        "registering":RegistrationForm,
        "type":"registre",

    }

    if request.method == "POST":
        registering = RegistrationForm(request.POST)
        if registering.is_valid():
            registering.save()
            #new_user_account = authenticate(
            #    username = registering.cleaned_data['username'],
            #    password = registering.cleaned_data['password1']
            #)
            #login(request, new_user_account)

            return redirect("login:logging_in")
        elif registering.errors:
            context["error"] = "There is something wrong with your inputs!"
    return render(request, 'login/register.html', context)

def logging_in(request):
    context = {"logging_in": LoginForm, "type":"login"}
    if request.user.is_authenticated:
        return redirect("book:book") #kalau udah login

    if request.method == "POST":
        logging_in = LoginForm(data = request.POST)
        if logging_in.is_valid():
            user_account = logging_in.get_user()
            login(request, user_account)
            return redirect("book:book") #kalo baru login
        elif logging_in.errors:
            context["error"] = "Wrong Username or Password"
    return render(request, 'login/login.html', context)

def logging_out(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("login:logging_in")
