from django.test import TestCase, Client
from django.contrib.auth.forms import UserCreationForm
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from django.apps import AppConfig
from django.apps import apps
from .views import *

# Create your tests here.
class UserCreationFormTest(TestCase):

    def test_form(self):
        data = {
            'username': 'testuser',
            'password1': 'testpassword123',
            'password2': 'testpassword123',
        }

        form = UserCreationForm(data)
        self.assertTrue(form.is_valid())

class test_path(TestCase):
    def test_path(self):
        response = Client().get('/login',{},True)
        self.assertEqual(response.status_code,200)
    def test_path2(self):
        response = Client().get('/register',{},True)
        self.assertEqual(response.status_code,200)
    def test_path3(self):
        response = Client().get('/logout',{},True)
        self.assertEqual(response.status_code,200)

class text_input(TestCase):
    def test_textHTMLLogin(self):
        response = Client().get('/login')
        html = response.content.decode('utf-8')
        self.assertIn("", html)
        self.assertIn('',html)

    def test_textHTMLDaftar(self):
        response = Client().get('/register')
        html = response.content.decode('utf-8')
        self.assertIn("",html)
        self.assertIn("", html)

class test_urls(TestCase):
    def setUp(self):
        self.login = reverse("login:login")
        self.register = reverse("login:register")
        self.logout = reverse("login:logout")

class ReportsConfig(AppConfig):
    def test_apps(self):
        self.assertEqual(ReportsConfig.name, 'login')
        self.assertEqual(apps.get_app_config('login').name, 'login')

#class loginFunctionTests(TestCase):
#    def test_url_register(self):
#        response= Client().get('/register')
#        self.assertEquals(response.status_code, 200)
    
#    def test_url_login(self):
#        response = Client().get('/login')
#        self.assertEquals(response.status_code, 301)

