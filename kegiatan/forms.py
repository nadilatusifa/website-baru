from django import forms
from .models import Kegiatan,Participant

class KegiatanForm(forms.ModelForm):
    namaKegiatan = forms.CharField(label='Nama Kegiatan : ')

    class Meta:
        model = Kegiatan
        fields = '__all__'
    
class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = '__all__'
