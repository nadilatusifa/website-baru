from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from .forms import KegiatanForm, ParticipantForm
from .models import Kegiatan, Participant
# Create your views here.

def index(request):
    keg = Kegiatan.objects.all()
    peserta = Participant.objects.all()
    keg_par = []
    for kegiatan in keg:
        kegiatan_partic = []

        for participant in peserta:
            if (str(participant.kegiatan) == str(kegiatan)):
                kegiatan_partic.append(participant)
        keg_par.append((kegiatan,kegiatan_partic))
        
    context= {
        'keg':keg,
        'keg_par' : keg_par,
        
    }
    return render(request, 'kegiatan/card_kegiatan.html', context)

def tambah(request):
    form = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/card_kegiatan')
    return render(request, 'kegiatan/kegiatan.html', {'form':form})


def tambah_peserta(request):
    form = ParticipantForm()
    if request.method == 'POST':
       form = ParticipantForm(request.POST)
       if form.is_valid():
           form.save()
           return redirect('/card_kegiatan')
    return render(request, 'kegiatan/form_peserta.html',{'form':form})


    
    
