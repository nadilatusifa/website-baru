from django.contrib import admin
from .models import Kegiatan, Participant
# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Participant)