from django.urls import path

from . import views
app_name = 'kegiatan'

urlpatterns = [
    path('card_kegiatan', views.index, name='card_kegiatan'),
    path('tambah', views.tambah, name='tambah'),
    path('tambah_peserta/', views.tambah_peserta, name='tambah_peserta'),


]