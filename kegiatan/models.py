from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length = 500)

    def __str__(self):
        return self.namaKegiatan

class Participant(models.Model):
    nama = models.CharField(max_length = 200)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.kegiatan) +"dan" + self.nama
