from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response
from kegiatan.models import Kegiatan,Participant

#from .models import Kegiatan

# Create your tests here.
class TestStory6(TestCase):
    def test_url_is_exist (self) :
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_tambah_is_exist(self):
        response = Client().get('/tambah')
        self.assertEqual(response.status_code, 200)

    def test_url_tambah_peserta_is_exist(self):
        response = Client().get('/tambah_peserta')
        self.assertEqual(response.status_code, 301)

    def test_create_models_kegiatan(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan='dance')
        counting = Kegiatan.objects.all().count()
        self.assertAlmostEquals(counting, 1)

    def test_create_models_participant(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan='dance')
        participant = Participant.objects.create(nama="nadila",kegiatan=kegiatan)
        self.assertEquals(str(participant),"dancedannadila")

    #def test_menyimpan_kegiatan_baru(self):
    #    kegiatan = Kegiatan.objects.create(namaKegiatan='dance')
    #    response = Client().get('/tambah',{}, True)
    #    count = Kegiatan.objects.all().count()
    #    self.assertEqual(count,1) 
    #    self.assertEqual(response.status_code, 200)


    #def test_penggunaan_template(self):
    #    kegiatan = Kegiatan.objects.create(namaKegiatan="Nama Kegiatan")
    #    participant = Participant.objects.create(nama="arya",kegiatan=kegiatan)

    #    response = Client().get('/card_kegiatan')
    #      self.assertTemplateUsed(response,'kegiatan/card_kegiatan.html')



    #def test_menyimpan_POST_participant_pada_kegiatan(self):
    #    kegiatan=Kegiatan.objects.create(namaKegiatan="kegiatan_test")
    #    response = Client().post('/tambah', data={
    #        "nama":"test", "kegiatan":kegiatan,
    #    })

    #    count = Participant.objects.all().count()
    #    self.assertEqual(count,1)
    #    self.assertEqual(response.status_code,302)
    